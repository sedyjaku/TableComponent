package factory;

import model.Division;
import model.Quarter;
import model.Table;
import model.Vendor;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.opencsv.CSVReader;

import config.TableFactoryConfig;

public class TableFactory extends AbstractTableFactory {
	private static TableFactory instance=null;
	
	
	public static AbstractTableFactory getInstance(){
        if(instance==null) instance=new TableFactory();
        return instance;
    }

	@Override
	public Table loadFromFile(String path) {
		Reader reader;
		Table table = new Table();
		try {
			reader = Files.newBufferedReader(Paths.get(path));
		} catch (IOException e) {
			Logger.getGlobal().log(Level.SEVERE, "Failed to load csv file", e);
			return table;
		}
        CSVReader csvReader = new CSVReader(reader);
        List<String[]> records=new ArrayList<>();
        try {
			records = csvReader.readAll();
			csvReader.close();
		} catch (IOException e) {
			Logger.getGlobal().log(Level.SEVERE, "Failed to parse csv", e);
			return table;
		}
        table.setColumnDescriptions(records.get(TableFactoryConfig.DESCRIPTION_ROW_INDEX));
        records.remove(TableFactoryConfig.DESCRIPTION_ROW_INDEX);
        for (String[] record : records) {
            Quarter quarter=table.findQuarter(record[TableFactoryConfig.TIMESCALE_INDEX]);
            if(quarter==null){
            	quarter=new Quarter(record[TableFactoryConfig.TIMESCALE_INDEX]);
            	table.addQuarter(record[TableFactoryConfig.TIMESCALE_INDEX], quarter);
            }
            String vendorName=record[TableFactoryConfig.VENDOR_INDEX];
            String country = record[TableFactoryConfig.COUNTRY_INDEX];
            double units=Double.parseDouble(record[TableFactoryConfig.UNITS_INDEX]);
            int vendorPosition=quarter.findVendor(vendorName);
            Vendor vendor;
            if(vendorPosition==TableFactoryConfig.VENDOR_NOT_FOUND){
            	vendor=new Vendor();
            	vendor.setVendorName(vendorName);
            	quarter.addVendor(vendor);
            }
            else{
            	vendor=quarter.getVendors().get(vendorPosition);
            }
            Division division=new Division();
            division.setCountry(country);
            division.setUnits(units);
            vendor.addDivision(division); 
        }
        return table;
	}
}
