package factory;

import model.Table;

public abstract class AbstractTableFactory {
	
	public abstract Table loadFromFile(String path);

}
