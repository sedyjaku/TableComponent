import java.util.List;

import model.Quarter;
import model.Table;
import model.Vendor;
import factory.TableFactory;


public class TableComponent {

	public static String PATH_TO_FILE="";
	public static String PATH_TO_HTML_EXPORT="";
	
	public static void main(String[] args) {
		Table table=TableFactory.getInstance().loadFromFile(PATH_TO_FILE);
		for(Quarter quarter: table.getQuarterMap().values()){
			
			quarter.countTotal();
			quarter.countPercentage();
			//sort quarters by units
			System.out.println(quarter.getName());
			List<Vendor> sortedList=quarter.sortByUnits();
			for (Vendor vendor: sortedList){
				System.out.println(vendor.getVendorName() + ", "+  vendor.getTotalShare() + ", " + vendor.getTotalUnits());
			}
			//sort quarters alphabetically
			System.out.println(quarter.getName());
			sortedList=quarter.sortAlphabeticaly();
			for (Vendor vendor: sortedList){
				System.out.println(vendor.getVendorName() + ", "+  vendor.getTotalShare() + ", " + vendor.getTotalUnits());
			}
			
			int vendorPosition = quarter.findVendor("Dell"); 
			System.out.println("DELL is at position " +(vendorPosition+1));
			Vendor vendor=quarter.getVendors().get(vendorPosition);
			System.out.println("Units: "+ vendor.getTotalUnits() + ", Share: " +vendor.getTotalShare()*100 + "%" );
			quarter.exportToHtml(PATH_TO_HTML_EXPORT+quarter.getName()+".html");
		}
		
	}
}
