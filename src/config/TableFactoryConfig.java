package config;

public class TableFactoryConfig {

	public static final int COUNTRY_INDEX = 0;
    public static final int TIMESCALE_INDEX = 1;
    public static final int VENDOR_INDEX = 2;
    public static final int UNITS_INDEX = 3;
    public static final int DESCRIPTION_ROW_INDEX=0;
    public static final int VENDOR_NOT_FOUND=-1;
}
