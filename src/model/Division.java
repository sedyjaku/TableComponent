package model;

/**
 * @author EnXer
 *
 */
public class Division {
	
	private String country;
	private double units;
	private double share;
		
	
	
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public double getUnits() {
		return units;
	}
	public void setUnits(double units) {
		this.units = units;
	}
	public double getShare() {
		return share;
	}
	public void setShare(double share) {
		this.share = share;
	}
	
	@Override
	public String toString() {
		return "\nRow [country=" + country +  ", units="
				+ units + ", share=" + share + "]";
	}
	
}
