package model;


import java.util.HashMap;
import java.util.Map;

public class Table {
	private Map<String,Quarter> quarterMap;
	private String[] columnDescriptions;

	



	public Table(){
		quarterMap=new HashMap<>();
	}
	

	/**
	 * adds quarter to quarter map
	 * @param quarterName name of the quarter
	 * @param quarter to be added into map
	 */
	public void addQuarter(String quarterName,Quarter quarter) {
		quarterMap.put(quarterName, quarter);
		
	}
	
	public Quarter findQuarter(String quarterName){
		return quarterMap.get(quarterName);
	}



	public Map<String, Quarter> getQuarterMap() {
		return quarterMap;
	}



	public void setQuarterMap(Map<String, Quarter> quarterMap) {
		this.quarterMap = quarterMap;
	}
	
	public String[] getColumnDescriptions() {
		return columnDescriptions;
	}



	public void setColumnDescriptions(String[] columnDescriptions) {
		this.columnDescriptions = columnDescriptions;
	}



}
