package model;

import java.util.ArrayList;
import java.util.List;

public class Vendor {
	String vendorName;
	List<Division> divisions;
	double totalShare;
	double totalUnits;
	
	public Vendor() {
		divisions=new ArrayList<>();
		totalShare=0;
	}
	
	/**
	 * Adds division to division list
	 * @param division to be added to the list
	 */
	public void addDivision(Division division){
		divisions.add(division);
	}
	
	public double getTotalShare() {
		return totalShare;
	}

	public void setTotalShare(double totalShare) {
		this.totalShare = totalShare;
	}

	
	public double getTotalUnits() {
		return totalUnits;
	}

	public void setTotalUnits(double totalUnits) {
		this.totalUnits = totalUnits;
	}
	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	
	public List<Division> getDivisions() {
		return divisions;
	}
	
	public void setDivisions(List<Division> divisions) {
		this.divisions = divisions;
	}
	
}
