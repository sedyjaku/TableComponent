package model;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import config.TableFactoryConfig;

/**
 * @author EnXer
 *
 */
/**
 * @author EnXer
 *
 */
public class Quarter {
	
	private List<Vendor> vendors;
	private Map<String,Integer> vendorPositionMap;
	

	private double total;
	private String name;
	
	public Quarter(String quarterName){
		total=0;
		vendors=new ArrayList<>();
		vendorPositionMap=new HashMap<>();
		name=quarterName;
	}
	
	/**
	 * counts total number of units in specific quarter
	 */
	public void countTotal(){
		total=0;
		for(Vendor vendor : vendors){
			for(Division div :  vendor.getDivisions()){
				total+=div.getUnits();
			}
		}
	}
	
	/**
	 * Counts share of each vendor and division based on its unit value compared to total quarter value
	 */
	public void countPercentage(){
		for(Vendor vendor : vendors){
			double totalVendorUnits=0;
			for(Division div :  vendor.getDivisions()){
				totalVendorUnits+=div.getUnits();
				div.setShare(div.getUnits()/total);
			}
			vendor.setTotalUnits(totalVendorUnits);
			vendor.setTotalShare(totalVendorUnits/total);
		}
	}
	
	/**
	 * Finds vendor name in row map
	 * @param name Vendor name to search for
	 * @return position of the input in list(starting from 1) or -1 if not found
	 */
	public int findVendor(String name){
		
		Integer position=vendorPositionMap.get(name);
		if(position==null)
		{
			return TableFactoryConfig.VENDOR_NOT_FOUND;
		}
		return position;
		 
	}
	
	/**
	 * Adds vendor to vendors list and also to position map
	 * @param vendor to be added to list
	 */
	public void addVendor(Vendor vendor){
		vendors.add(vendor);
		vendorPositionMap.put(vendor.getVendorName(), vendorPositionMap.size());
	}
	
	/**
	 * Sorts new list alphabeticaly by vendor in rows
	 * @return sorted list
	 */
	public List<Vendor> sortAlphabeticaly(){
		List<Vendor> sortedList=new ArrayList<>(vendors);
		Collections.sort(sortedList, new Comparator<Vendor>() {
		    @Override
		    public int compare(Vendor lhs, Vendor rhs) {
		    	return lhs.getVendorName().compareTo(rhs.getVendorName());
		    }
		});
		
		return sortedList;
	}
	
	/**
	 * Sorts new list alphabeticaly by vendor in rows
	 * @return sorted list
	 */
	public List<Vendor> sortByUnits(){
		List<Vendor> sortedList=new ArrayList<>(vendors);
		Collections.sort(sortedList, new Comparator<Vendor>() {
		    @Override
		    public int compare(Vendor lhs, Vendor rhs) {
		    	return (int) (lhs.getTotalUnits()-rhs.getTotalUnits());
		    }
		});
		
		return sortedList;
	}
	/**
	 * exports data from quarter object to html file
	 * @param path path to file
	 */
	public void exportToHtml(String path){
		String page="<html>"
				+ "		<head>"
				+ "			<title>"+ name+ "</title>"
				+ "<style>"
				+ "table {"
				+ "border-collapse: collapse;"
				+ "}"
				+ "	table, th, td {"
			    + "	border: 1px solid black;"
			    + "	}"
					+ "	</style>"
				+ "		</head>"
				+ "		<body>";
		page+=" 			<table align=\"center\" style=\"width:50%\">"
				+ "				<tr bgcolor=\"grey\">"
				+ "					<th align=\"center\">Vendor</td>"
				+ "					<th align=\"center\">Units</td>"
				+ "					<th align=\"center\"><b>Share</b></td>"
				+ "				</tr>";
		
		for(Vendor vendor: vendors){
			page+="				<tr>"
				+ "					<td align=\"center\">"+vendor.vendorName+"</td>"
				+ "					<td align=\"center\">"+String.format("%.3f", vendor.getTotalUnits())+"</td>"
				+ "					<td align=\"center\">"+String.format("%.1f", vendor.getTotalShare()*100)+"%</td>"
				+ "				</tr>";
		}
		
		page+="				<tr bgcolor=\"yellow\">"
				+ "					<td align=\"center\">Total</td>"
				+ "					<td align=\"center\">"+String.format("%.3f",total)+"</td>"
				+ "					<td align=\"center\">100%</td>"
				+ "				</tr>";
		
		page+=" 			</table>";
		page+="		</body>"
		+ "</html>";
		PrintWriter writer;
		try {
			writer = new PrintWriter(path, "UTF-8");
		} catch (FileNotFoundException e) {
			Logger.getGlobal().log(Level.SEVERE, "File not found", e);
			return;
		} catch (UnsupportedEncodingException e) {
			Logger.getGlobal().log(Level.SEVERE, "Unknown encoding", e);
			return;
		}
		writer.println(page);
		writer.close();
	}
	
	/**
	 * exports data from quarter object to csv similar to html one
	 * @param path path to file
	 */
	public void exportToCSV(String path){
	
	}
	
	/**
	 * exports data from quarter object to xls file similiar to html one using apache poi library (http://poi.apache.org/)
	 * @param path path to file
	 */
	public void exportToXls(String path){
	
	}
	
	

	public List<Vendor> getVendors() {
		return vendors;
	}

	public void setVendors(List<Vendor> vendors) {
		this.vendors = vendors;
	}

	public Map<String, Integer> getVendorPositionMap() {
		return vendorPositionMap;
	}

	public void setVendorPositionMap(Map<String, Integer> vendorPositionMap) {
		this.vendorPositionMap = vendorPositionMap;
	}


	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "Quarter [total=" + total + ", name="
				+ name + ", vendors=" + vendors + "]";
	}
	
	

}
